<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="34"/>
        <source>About PDF Mix Tool</source>
        <translation>Über PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="44"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="54"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="66"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation>Eine Anwendung zum Aufteilen, Zusammenfügen, Drehen und Mischen von PDF-Dateien.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="67"/>
        <source>Website</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="74"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="90"/>
        <source>Authors</source>
        <translation>Autoren</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="92"/>
        <source>Translators</source>
        <translation>Übersetzer</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="101"/>
        <source>Credits</source>
        <translation>Beiträge</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="119"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>Submit a pull request</source>
        <translation>Sende einen Pull-Request</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="130"/>
        <source>Report a bug</source>
        <translation>Melde einen Fehler</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="131"/>
        <source>Help translating</source>
        <translation>Beim Übersetzen helfen</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="138"/>
        <source>Contribute</source>
        <translation>Beitragen</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="154"/>
        <source>Changelog</source>
        <translation>Änderungsprotokoll</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Ändere Mehrseiten-Profil</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="60"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="158"/>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="61"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <source>Center</source>
        <translation>Zentriert</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="62"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="160"/>
        <source>Right</source>
        <translation>Rechts</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="162"/>
        <source>Top</source>
        <translation>Oben</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="164"/>
        <source>Bottom</source>
        <translation>Unten</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="97"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="105"/>
        <source>Output page size</source>
        <translation>Papierformat des Output</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="107"/>
        <source>Standard size:</source>
        <translation>Standardgröße:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="110"/>
        <source>Custom size:</source>
        <translation>Eigene Größe:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="113"/>
        <source>Width:</source>
        <translation>Breite:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="116"/>
        <source>Height:</source>
        <translation>Höhe:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="124"/>
        <source>Pages layout</source>
        <translation>Seiten-Layout</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="126"/>
        <source>Rows:</source>
        <translation>Zeilen:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Columns:</source>
        <translation>Spalten:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rotation:</source>
        <translation>Drehung:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Spacing:</source>
        <translation>Abstand:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="143"/>
        <source>Pages alignment</source>
        <translation>Seiten ausrichten</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="145"/>
        <source>Horizontal:</source>
        <translation>Horizontal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="148"/>
        <source>Vertical:</source>
        <translation>Vertikal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="156"/>
        <source>Margins</source>
        <translation>Ränder</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="98"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="169"/>
        <source>portrait</source>
        <translation>Portrait</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="98"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="169"/>
        <source>landscape</source>
        <translation>Quer</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="109"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="177"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/inputpdffiledelegate.cpp" line="99"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="170"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n Seite(n)</numerusform>
            <numerusform>%n Seiten</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="111"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="179"/>
        <source>Pages:</source>
        <translation>Seiten:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="112"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="180"/>
        <source>Multipage:</source>
        <translation type="unfinished">Mehrseiten:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="115"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="183"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="116"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="184"/>
        <source>Rotation:</source>
        <translation>Drehung:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="170"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="183"/>
        <source>No rotation</source>
        <translation>Keine Drehung</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="189"/>
        <source>Pages:</source>
        <translation>Seiten:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="191"/>
        <source>Multipage:</source>
        <translation type="unfinished">Mehrseiten</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="193"/>
        <source>Rotation:</source>
        <translation>Drehung:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="88"/>
        <source>Add PDF file</source>
        <translation>PDF-Datei hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="89"/>
        <source>Move up</source>
        <translation>Nach oben</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="90"/>
        <source>Move down</source>
        <translation>Nach unten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="91"/>
        <source>Remove file</source>
        <translation>Datei entfernen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="93"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="94"/>
        <source>Generate PDF</source>
        <translation>PDF-Datei erzeugen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="103"/>
        <source>PDF Mix Tool</source>
        <translation>PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="310"/>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>PDF generation error</source>
        <translation>Fehler beim Erzeugen des PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="192"/>
        <source>Select one or more PDF files to open</source>
        <translation>Wähle ein oder mehrere PDF-Dateien zum Öffnen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="92"/>
        <source>Multipage profiles…</source>
        <translation>Mehrseiten-Profile…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <location filename="../src/mainwindow.cpp" line="401"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF-Dateien (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="301"/>
        <source>Output pages: %1</source>
        <translation>Ausgabe-Seiten: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>You must add at least one PDF file.</source>
        <translation>Sie müssen mindestens eine PDF-Datei hinzufügen.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="334"/>
        <source>&lt;li&gt;Invalid character &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;Unzulässiges Zeichen&quot;&lt;b&gt;%1&lt;/b&gt;&quot; in Seiten-Filter von Datei &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="340"/>
        <source>&lt;li&gt;Invalid interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;Unzulässiges Intervall &quot;&lt;b&gt;%1&lt;/b&gt;&quot; im Seiten-Filter der Datei &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="346"/>
        <source>&lt;li&gt;Boundaries of interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; are out of allowed interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;Grenzen des Intervalls &quot;&lt;b&gt;%1&lt;/b&gt;&quot; im Seiten-Filter der Datei &quot;&lt;b&gt;%2&lt;/b&gt;&quot; sind außerhalb des erlaubten Intervalls&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="365"/>
        <source>&lt;li&gt;Interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; is overlapping with another interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;Intervall &quot;&lt;b&gt;%1&lt;/b&gt;&quot; im Seiten-Filter der Datei &quot;&lt;b&gt;%2&lt;/b&gt;&quot; überlappt mit einem anderen Intervall&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="375"/>
        <source>&lt;p&gt;The PDF generation failed due to the following errors:&lt;/p&gt;</source>
        <translation>&lt;p&gt;Das Erzeugen des PDF scheiterte wegen folgender Fehler:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="383"/>
        <source>&lt;p&gt;The following problems were encountered while generating the PDF file:&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die folgenden Probleme sind aufgetreten während das PDF erzeugt wurde:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="399"/>
        <source>Save PDF file</source>
        <translation>PDF-Datei abspeichern</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>Neues Profil…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="32"/>
        <source>Delete profile</source>
        <translation>Profil löschen</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="37"/>
        <source>Manage multipage profiles</source>
        <translation>Verwalte Mehrseiten-Profile</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="94"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="96"/>
        <source>Custom profile</source>
        <translation>Eigenes Profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <source>Profile name can not be empty.</source>
        <translation>Profilname darf nicht leer sein.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="147"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Profile name already exists.</source>
        <translation>Der Profilname existiert bereits.</translation>
    </message>
</context>
</TS>
