<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="34"/>
        <source>About PDF Mix Tool</source>
        <translation>Om PDF Mix-verktøyet</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="44"/>
        <source>Close</source>
        <translation>Lukk</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="54"/>
        <source>Version %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="66"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation>Et program for å splitte, flette, rotere og mikse PDF-filer.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="67"/>
        <source>Website</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="74"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="90"/>
        <source>Authors</source>
        <translation>Utviklere</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="92"/>
        <source>Translators</source>
        <translation>Oversettelse ved</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="101"/>
        <source>Credits</source>
        <translation>Bidragsytere</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="119"/>
        <source>License</source>
        <translation>Lisens</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>Submit a pull request</source>
        <translation>Send inn en flettingsforespørsel</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="130"/>
        <source>Report a bug</source>
        <translation>Innrapporter en feil</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="131"/>
        <source>Help translating</source>
        <translation>Hjelp til i oversettelsen</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="138"/>
        <source>Contribute</source>
        <translation>Bidra</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="154"/>
        <source>Changelog</source>
        <translation>Endringslogg</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Rediger flersidig profil</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="60"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="158"/>
        <source>Left</source>
        <translation>Venstre</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="61"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <source>Center</source>
        <translation>Senter</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="62"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="160"/>
        <source>Right</source>
        <translation>Høyre</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="162"/>
        <source>Top</source>
        <translation>Topp</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="164"/>
        <source>Bottom</source>
        <translation>Bunn</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="97"/>
        <source>Name:</source>
        <translation>Navn:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="105"/>
        <source>Output page size</source>
        <translation>Utdata-sidestørrelse</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="107"/>
        <source>Standard size:</source>
        <translation>Forvalgt størrelse:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="110"/>
        <source>Custom size:</source>
        <translation>Egendefinert størrelse:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="113"/>
        <source>Width:</source>
        <translation>Bredde:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="116"/>
        <source>Height:</source>
        <translation>Høyde:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="124"/>
        <source>Pages layout</source>
        <translation>Sideoppsett</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="126"/>
        <source>Rows:</source>
        <translation>Rader:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Columns:</source>
        <translation>Kolonner:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rotation:</source>
        <translation>Sideretning:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Spacing:</source>
        <translation>Avstand:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="143"/>
        <source>Pages alignment</source>
        <translation>Sidejustering</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="145"/>
        <source>Horizontal:</source>
        <translation>Horisontal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="148"/>
        <source>Vertical:</source>
        <translation>Vertikal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="156"/>
        <source>Margins</source>
        <translation>Marg</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="98"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="169"/>
        <source>portrait</source>
        <translation>stående</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="98"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="169"/>
        <source>landscape</source>
        <translation>liggende</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="109"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="177"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/inputpdffiledelegate.cpp" line="99"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="170"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n side</numerusform>
            <numerusform>%n sider</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="111"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="179"/>
        <source>Pages:</source>
        <translation>Sider:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="112"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="180"/>
        <source>Multipage:</source>
        <translation>Flersidig:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="115"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="183"/>
        <source>Disabled</source>
        <translation>Avskrudd</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="116"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="184"/>
        <source>Rotation:</source>
        <translation>Sideretning:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="170"/>
        <source>Disabled</source>
        <translation>Avskrudd</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="183"/>
        <source>No rotation</source>
        <translation>Ingen rotasjon</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="189"/>
        <source>Pages:</source>
        <translation>Sider:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="191"/>
        <source>Multipage:</source>
        <translation>Flersidig:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="193"/>
        <source>Rotation:</source>
        <translation>Sideretning:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="88"/>
        <source>Add PDF file</source>
        <translation>Legg til PDF-fil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="89"/>
        <source>Move up</source>
        <translation>Flytt oppover</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="91"/>
        <source>Remove file</source>
        <translation>Fjern fil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="93"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="94"/>
        <source>Generate PDF</source>
        <translation>Generer PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="103"/>
        <source>PDF Mix Tool</source>
        <translation>PDF Mix-verktøyet</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="192"/>
        <source>Select one or more PDF files to open</source>
        <translation>Velg én eller flere PDF-filer å åpne</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <location filename="../src/mainwindow.cpp" line="401"/>
        <source>PDF files (*.pdf)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="399"/>
        <source>Save PDF file</source>
        <translation>Lagre PDF-fil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="301"/>
        <source>Output pages: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="310"/>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>PDF generation error</source>
        <translation>PDF-genereringsfeil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>You must add at least one PDF file.</source>
        <translation>Du må legge til minst én PDF-fil.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="375"/>
        <source>&lt;p&gt;The PDF generation failed due to the following errors:&lt;/p&gt;</source>
        <translation>&lt;p&gt;PDF-genereringen mislyktes på grunn av disse feilene:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="334"/>
        <source>&lt;li&gt;Invalid character &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;Ugyldig tegn &quot;&lt;b&gt;%1&lt;/b&gt;&quot; i sidefilteret til filen &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="90"/>
        <source>Move down</source>
        <translation>Flytt nedover</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="92"/>
        <source>Multipage profiles…</source>
        <translation>Flersidige profiler…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="340"/>
        <source>&lt;li&gt;Invalid interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</source>
        <translation>&lt;li&gt;Ugyldig intervall &quot;&lt;b&gt;%1&lt;/b&gt;&quot; i sidefilteret til filen &quot;&lt;b&gt;%2&lt;/b&gt;&quot;&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="346"/>
        <source>&lt;li&gt;Boundaries of interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; are out of allowed interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;Yttergrensene for intervallet &quot;&lt;b&gt;%1&lt;/b&gt;&quot; i sidefilteret til filen &quot;&lt;b&gt;%2&lt;/b&gt;&quot; er utenfor tillatt intervall&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="383"/>
        <source>&lt;p&gt;The following problems were encountered while generating the PDF file:&lt;/p&gt;</source>
        <translation>&lt;p&gt;Støtte på disse feilene under generering av PDF-fil:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="365"/>
        <source>&lt;li&gt;Interval &quot;&lt;b&gt;%1&lt;/b&gt;&quot; in pages filter of file &quot;&lt;b&gt;%2&lt;/b&gt;&quot; is overlapping with another interval&lt;/li&gt;</source>
        <translation>&lt;li&gt;Intervall &quot;&lt;b&gt;%1&lt;/b&gt;&quot; i sidefilteret til filen&quot;&lt;b&gt;%2&lt;/b&gt;&quot; overlapper med et annet intervall&lt;/li&gt;</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>Ny profil…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="32"/>
        <source>Delete profile</source>
        <translation>Slett profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="37"/>
        <source>Manage multipage profiles</source>
        <translation>Behandle flersidige profiler</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="94"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="96"/>
        <source>Custom profile</source>
        <translation>Egendefinert profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="142"/>
        <source>Profile name can not be empty.</source>
        <translation>Profilnavnet kan ikke være tomt.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="147"/>
        <source>Disabled</source>
        <translation>Avskrudd</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="149"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="157"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Profile name already exists.</source>
        <translation>Profilnavnet finnes allerede.</translation>
    </message>
</context>
</TS>
